package controllers

import javax.inject._
import lazno.shared.SharedText
import play.api.Configuration
import play.api.http.HttpErrorHandler
import play.api.mvc._

/**
  * Frontend controller managing all static resource associate routes.
  * @param assets Assets controller reference.
  * @param cc Controller components reference.
  */
@Singleton
class FrontendController @Inject()(assets: Assets, errorHandler: HttpErrorHandler, config: Configuration, cc: ControllerComponents) extends AbstractController(cc) {

  def root: Action[AnyContent] = assets.at("index.html")

  def rootRedirect: Action[AnyContent] = Action{Redirect(controllers.routes.FrontendController.root())}

  def assetOrDefault(resource: String): Action[AnyContent] = assets.at(resource)

  def apiText= Action {Ok(SharedText.output())}

}