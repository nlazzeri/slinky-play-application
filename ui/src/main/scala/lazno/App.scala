package lazno

import lazno.shared.SharedText
import slinky.core._
import slinky.core.annotations.react
import slinky.web.html._

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

@JSImport("resources/styles/App.scss", JSImport.Default)
@js.native
object AppCSS extends js.Object

@JSImport("resources/images/react.svg", JSImport.Default)
@js.native
object ReactLogo extends js.Object

@JSImport("resources/images/play.svg", JSImport.Default)
@js.native
object PlayLogo extends js.Object
@JSImport("resources/images/slinky.svg", JSImport.Default)
@js.native
object SlinkyLogo extends js.Object

@react class App extends StatelessComponent {
  type Props = Unit

  private val css = AppCSS

  def render() = {
    div(className := "App")(
      header(className := "App-header")(
        img(src := PlayLogo.asInstanceOf[String], className := "Standard-logo", alt := "logo"),
        img(src := ReactLogo.asInstanceOf[String], className := "Rotating-logo", alt := "logo"),
        img(src := SlinkyLogo.asInstanceOf[String], className := "Standard-logo", alt := "logo"),
        h1(className := "App-title")(
          "Welcome to React (with Play, Scala.js and ",
          a(href := "https://slinky.shadaj.me/")("Slinky"),
          ")"
        )
      ),
      p(className := "App-intro")(
        "To get started, edit ", code("App.scala"), " and save to reload."
      ),
      p(className := "App-intro")(
        SharedText.output()
      )

    )
  }
}
