var path = require("path");
var merge = require('webpack-merge');
var core = require('./webpack-core.config.js')
var webpack = require("webpack");
var MiniCssExtractPlugin = require("mini-css-extract-plugin");
var OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");


var generatedConfig = require("./scalajs.webpack.config.js");
var entries = {};
entries[Object.keys(generatedConfig.entry)[0]] = "scalajs";

module.exports = merge(core, {
  mode: "production",
  entry: entries,
  output: {
    path: path.resolve(__dirname, "../../../../build/public")
  },
  optimization: {
    minimizer: [
      new OptimizeCSSAssetsPlugin({})
    ]
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          "css-loader",
          "sass-loader"
        ]
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    new MiniCssExtractPlugin({
      filename: 'stylesheets/[name].[hash].css',
      chunkFilename: 'stylesheets/[id].[hash].css'
    })
  ]
})
