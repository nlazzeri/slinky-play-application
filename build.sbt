import FrontendBuildUtils._
import com.typesafe.sbt.packager.universal.UniversalPlugin.autoImport.stage
import sbt.Keys.{version, _}
import sbt.addCommandAlias
import sbtcrossproject.CrossPlugin.autoImport.{CrossType, crossProject}
import scalajsbundler.sbtplugin.ScalaJSBundlerPlugin.autoImport.{npmDevDependencies, webpackBundlingMode, webpackConfigFile, webpackDevServerExtraArgs}

//cross compilation jvm and scalajs
lazy val shared = (crossProject(JSPlatform, JVMPlatform).crossType(CrossType.Pure) in file("shared"))
  .settings(
    scalaVersion := Settings.versions.scala,
    name := "shared",
    version := Settings.version
  )
  .jsSettings()
  .jvmSettings()

lazy val sharedJVM = shared.jvm
lazy val sharedJS = shared.js

//ui project with scalajs
lazy val ui = (project in file("ui"))
  .settings(
    name := "ui",
    version := Settings.version,
    scalaVersion := Settings.versions.scala,
    scalacOptions += Settings.scalacOptionsScalajs,

    //scalajs dependencies
    libraryDependencies ++= Seq(
      "me.shadaj" %%% "slinky-web" % Settings.versions.slinky,
      "me.shadaj" %%% "slinky-hot" % Settings.versions.slinky
    ),

    //js dependencies
    npmDependencies in Compile ++= Seq(
      "react" -> Settings.versions.react,
      "react-dom" -> Settings.versions.react,
      "react-proxy" -> Settings.versions.reactProxy
    ),

    //webpack dependencies
    npmDevDependencies in Compile ++= Seq(
      "file-loader" -> Settings.versions.fileLoader,
      "style-loader" -> Settings.versions.styleLoader,
      "css-loader" -> Settings.versions.cssLoader,
      "node-sass" -> Settings.versions.nodeSass,
      "sass-loader" -> Settings.versions.sassLoader,
      "html-webpack-plugin" -> Settings.versions.htmlWebpack,
      "copy-webpack-plugin" -> Settings.versions.copyWebpack,
      "clean-webpack-plugin" -> Settings.versions.cleanWebpack,
      "webpack-merge" -> Settings.versions.webpackMerge,
      "mini-css-extract-plugin" -> Settings.versions.miniCssExtract,
      "optimize-css-assets-webpack-plugin" -> Settings.versions.optimizeCssAssetsWebpack
    ),

    version in webpack := Settings.versions.webpack,
    version in startWebpackDevServer := Settings.versions.startWebpackDevServer,
    webpackResources := baseDirectory.value / "webpack" * "*",
    webpackConfigFile in fastOptJS := Some(baseDirectory.value / "webpack" / "webpack-fastopt.config.js"),
    webpackConfigFile in fullOptJS := Some(baseDirectory.value / "webpack" / "webpack-opt.config.js"),
    webpackConfigFile in Test := Some(baseDirectory.value / "webpack" / "webpack-core.config.js"),
    webpackDevServerExtraArgs in fastOptJS := Seq("--inline", "--hot"),
    webpackBundlingMode in fastOptJS := BundlingMode.LibraryOnly(),
    requireJsDomEnv in Test := true,

    //TODO maybe this is not needed anymore with newer sbt and scala versions
    addCompilerPlugin("org.scalamacros" % "paradise" % Settings.versions.paradiseMacro cross CrossVersion.full)
  )
  .enablePlugins(ScalaJSBundlerPlugin)
  .dependsOn(sharedJS)

lazy val `ui-prod-build` = taskKey[Unit]("Run UI build when packaging the application.")
lazy val root = (project in file("."))
  .settings(
    name := "slinky-play-application",
    version := Settings.version,
    scalaVersion := Settings.versions.scala,
    libraryDependencies += guice,
    resolvers += Resolver.sonatypeRepo("snapshots"),

    //when building the root project, also build ui
    `ui-prod-build` := {
      implicit val uiNpmTargetDir: File = baseDirectory.value
      if (executeProdBuild != Success) throw new Exception("Oops! UI Build crashed.")
    },
    dist := (dist dependsOn `ui-prod-build`).value,
    stage := (stage dependsOn `ui-prod-build`).value,
    resourceDirectory in Assets := baseDirectory.value / "ui/build/public",

    //no play pid file needed as this will be the only app inside a docker container
    javaOptions in Universal ++= Seq(
      "-Dpidfile.path=/dev/null"
    ),

    //some aliases for ui build
    addCommandAlias("runui", FrontendCommands.serve),
    addCommandAlias("runUi", FrontendCommands.serve),
    addCommandAlias("distui", FrontendCommands.build),
    addCommandAlias("distUi", FrontendCommands.build)
  )
  .enablePlugins(PlayScala, DockerPlugin)
  .dependsOn(sharedJVM)
  .aggregate(sharedJS, sharedJVM)
