import sbt.File

import scala.sys.process
import scala.sys.process.Process

object FrontendBuildUtils {
  // Execution status success.
  val Success = 0

  // Execution status failure.
  val Error = 1

  val isWindows = System.getProperty("os.name").toLowerCase().contains("win")

  def createProcessFor(script: String)(implicit dir: File): process.ProcessBuilder = {
    if (isWindows) {
      Process("cmd /c set CI=true&& " + script, dir)
    } else {
      Process("env CI=true " + script, dir)
    }
  }

  def runOnCommandline(script: String)(implicit dir: File): Int = {
    createProcessFor(script)
  } !
  
  def executeProdBuild(implicit dir: File): Int = runOnCommandline("sbt \"" + FrontendCommands.build + "\"")

}
