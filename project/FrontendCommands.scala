/**
  * Frontend build commands.
  */
object FrontendCommands {
  val serve: String = ";project ui;fastOptJS::startWebpackDevServer;~fastOptJS"
  val build: String = ";project ui;fullOptJS::webpack"
}