object Settings {
  val version = "1.0.0"

  val scalacOptionsScalajs = "-P:scalajs:sjsDefinedByDefault"

  object versions {
    val scala = "2.12.8"
    //scalajs
    val scalaDom = "0.9.3"
    val scalajsReact = "1.4.0"
    val scalaCSS = "0.5.3"
    //js
    val react = "16.7.0"
    val reactProxy = "1.1.8"
    val slinky = "0.5.2"
    
    //webpack
    val webpack = "4.29.0"
    val startWebpackDevServer = "3.1.14"
    val fileLoader = "3.0.1"
    val styleLoader = "0.23.1"
    val cssLoader = "2.1.0"
    val nodeSass = "4.11.0"
    val sassLoader = "7.1.0"
    val miniCssExtract = "0.5.0"
    val htmlWebpack = "3.2.0"
    val webpackMerge = "4.2.1"
    val copyWebpack = "4.6.0"
    val cleanWebpack = "1.0.1"
    val optimizeCssAssetsWebpack = "5.0.1"
   
    //intellij integration
    val paradiseMacro = "2.1.1"
  }


}
