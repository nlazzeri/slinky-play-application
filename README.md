# Example Project ScalaJS/React(Slinky) and Play

[![Scala.js](https://www.scala-js.org/assets/badges/scalajs-0.6.17.svg)](https://www.scala-js.org)

Example Project integrating
[Scala.js](http://www.scala-js.org/), [Play](https://www.playframework.com/), [React](https://reactjs.org/) using the
 [Slinky](https://slinky.shadaj.me/) React Facade

## Purpose

This project integrates ScalaJs, React and Play in a way that offers a nice developing 
experience. My Goals where:

1. hot reloading on front and backend while developing (scalajs-bundler, webpack and playframework)
2. single build step with an executable or docker image reade to be used (sbt-native-packager)
3. type safety while developing a SAP in javascript (scalaJS)
4. reactJS for developtment (Slinky)
5. a strong foundation for my rest-api, with non blocking IO (playframework)
6. shared code between frontend and backend for validations, etc.. (CrossProject) 

## Results

For the most part I am satisfied with the results. In this version one has to start a separate process for webpack 
and play. Initially my intend was to run backend and frontend with a single command. However this was impractical, as
 changes to my frontend code sometimes did not compile but errors didnt show up in the console. Also I realized its 
 much nicer to have more control what parts of your application are actually running at any given moment.
 
 Anyway it would be quite easy to startup webpack too using a Play RunHook.
 
 IntelliJ integration somehow needs an extra step to pick up the Slinky @react macro annotation, one has to add the 
 scala extension manually.
 
 Cross compiled code needs 2 steps to be published into the running webpack application. First a compile has to be 
 triggered at the root project. Then any change to existing scalajs code has to be made, so webpack picks up the new 
 cross compiled javascript code. Not really bad, just something to keep in mind when making code changes
 
 Other than that its quite nice! Hot realoading works like a charm, I was surprised how fast changes in my ScalaJS 
 code are compiled and visible in my browser.
 
 ## Usage
 
 ### Structure
 The project is separated into 3 sbt projects:
 1. "shared" which compiles code into scalaVM and scalaJS code
 2. "ui" contains all ScalaJS code
 3. "slinky-play-application" is the root project which also contains the play app
 
 For easy transition from any existing play application I kept the play directory layout. I dont particularly like 
 this directory layout and generally prefer the maven way of structuring code. Still wanted this project to be as 
 "Play"-like as possible.
 
 ### Folder layout:
 
 app --> play application
 
 conf --> play configuration
 
 project --> sbt configs
 
 shared --> shared scala code. all shared code goes to src/main/scala
 
 ui --> subproject with maven layout containing scalaJS code
 
 ### Running and building
  
 "sbt run" --> runs backend on port localhost:9000
 
 "sbt dist" --> build distribution zipped in /target/universal
 
 "sbt docker:publishLocal" --> publishes a docker image to your local docker registry
 
 "sbt distui" --> build frontend only, finished build can be found in ui/build folder
 
 "sbt runui" --> runs frontend and serves on localhost:8080
 
 the backend serves assets from the ui/build folder. this way you can run your backend and still see your running 
 application at localhost:9000/. This is quite nice to check if you didnt screw your routes, security headers and so 
 on. Just build your frontend and any changes will be visible in the browser on reload.
 
 ## Credits
 
 Thanks to Yohan Gomez for having an awesome [Seed Project](https://github.com/yohangz/scala-play-react-seed). 
 
 Same goes to Otto Chrons for his [SPA Tutorial](https://github.com/ochrons/scalajs-spa-tutorial) (which I took as a base project)
 